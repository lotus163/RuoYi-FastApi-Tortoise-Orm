# FastApi+Tortoise-ORM实现的若依后台管理系统

### 项目简介

---------------------------------------------------------
* 本项目为若依跟后台管理系统的FastApi版本
* 前端依靠[RuoYi-Vue3](https://github.com/yangzongzhuan/RuoYi-Vue3)，只有一处修改。
* 后端主要依靠FastApi+Tortoise-ORM+AioRedis
* 数据管理写的很简单，只做了部门和用户。
* 权限管理也写的很简单。
* 由于本人水平有限，欢迎指正。

### 感谢

----------------------------------------------------------
* [若依](http://www.ruoyi.vip/)
* [RuoYi-Vue3](https://github.com/yangzongzhuan/RuoYi-Vue3)
* [学生选课系统](https://gitee.com/zxiaosi/fast-api/tree/master/)
* [小菠萝测试笔记](https://www.cnblogs.com/poloyy/tag/FastAPI/)

### 前端安装
* 使用的[RuoYi-Vue3](https://github.com/yangzongzhuan/RuoYi-Vue3)，只需要在.env.development中改为:
VITE_APP_BASE_API = 'http://localhost:8001/dev-api'

### 后端安装

* 进入到 backend 项目下
* 执行 pip install -r requirements.txt
* 在mysql中新建数据库，字符集选择mb4，排序mb4-general-ci,将register\tortoise.py中第30行改为generate_schemas=True。
* 修改config文件夹下的setting中的Database部分和Redis部分
* 找到 main.py 右键运行（建议用Pycharm启动）
* mysql数据表生成后，再使用sql文件夹中的东西导入
* 接口文档:http://127.0.0.1:8001/dev-api/docs

### casbin
初始化了casbin，但未使用。安装时候只需要安装asynccasbin和casbin_tortoise_adapter

### 权限管理
目前还没做


有bug或问题，可以发送邮件lotus163@163.com

