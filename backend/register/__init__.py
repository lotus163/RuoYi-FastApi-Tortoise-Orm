#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File: __init__.py.py
# @Author: lotus163
# @Date: 2022/11/21

from backend.register.casbin import register_casbin
from backend.register.cors import register_cors
from backend.register.exception import register_exception
from backend.register.log import logger_init
from backend.register.middleware import register_middleware
from backend.register.mount import register_mount
from backend.register.redis import register_redis
from backend.register.router import register_router
from backend.register.tortoise import register_orm
