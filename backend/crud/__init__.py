#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/2 12:06
# file: __init__.py.py
# author: lotus163
from .base import CRUDBase
from ..models import SysConfigDb, SysDictTypeDb, SysDictDataDb, SysNoticeDb, SysUserDb, SysRoleDb, SysMenuDb, SysDeptDb, \
    SysPostDb, LoginInforDb, OperLogDb
from ..schemas import SysConfigSch, SysDictTypeSch, SysDictDataSch, SysNoticeSch, SysUserSch, SysRoleSch, SysMenuSch, \
    SysDeptSch, SysPostSch, LoginInforSch, OperLogSch

SysConfigDao = CRUDBase(SysConfigDb, SysConfigSch)

DictTypeDao = CRUDBase(SysDictTypeDb, SysDictTypeSch)

DictDataDao = CRUDBase(SysDictDataDb, SysDictDataSch)

NoticeDao = CRUDBase(SysNoticeDb, SysNoticeSch)

UserDao = CRUDBase(SysUserDb, SysUserSch)

RoleDao = CRUDBase(SysRoleDb, SysRoleSch)

MenuDao = CRUDBase(SysMenuDb, SysMenuSch)

DeptDao = CRUDBase(SysDeptDb, SysDeptSch)

PostDao = CRUDBase(SysPostDb, SysPostSch)

LoginInforDao = CRUDBase(LoginInforDb, LoginInforSch)

OperLogDao = CRUDBase(OperLogDb, OperLogSch)
