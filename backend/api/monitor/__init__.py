#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: login.py
# author: lotus163
from fastapi import APIRouter

from backend.api.monitor import cache, server, login_infor, oper_log, user_online

router = APIRouter(prefix="/monitor")
router.include_router(cache.router)
router.include_router(server.router)
router.include_router(login_infor.router)
# 操作日志
router.include_router(oper_log.router)
router.include_router(user_online.router)
