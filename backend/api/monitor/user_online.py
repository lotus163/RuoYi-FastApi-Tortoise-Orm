#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: user.py
# author: lotus163
from typing import Optional

from fastapi import APIRouter, Depends

from ...core.deps import UserDepends, get_user_depends_nolog, get_user_depends
from ...core.result import Result, result_success, ResultList
from ...schemas import UserOnlineSch

router = APIRouter(prefix="/online", tags=["在线用户"])


@router.get("/list", response_model=ResultList[UserOnlineSch], summary='获取在线用户列表')
async def get_user_online(ipaddr: Optional[str] = None, userName: Optional[str] = None,
                          user_depends: UserDepends = Depends(get_user_depends_nolog)):
    # 准备查询的参数
    form_dict = {}
    if ipaddr:
        form_dict["ipaddr"] = ipaddr
    if userName:
        form_dict["userName"] = userName
    # 查询
    total, users_online_sch = await user_depends.redis.get_users_online_sch()
    return result_success(msg="查询成功", rows=users_online_sch, total=total)


@router.delete("/{tokenId}", response_model=Result, summary='根据{tokenId}强退用户')
async def force_logout(tokenId: str, user_depends: UserDepends = Depends(get_user_depends)):
    await user_depends.redis.delete_login_tokens(tokenId)
    return result_success()
