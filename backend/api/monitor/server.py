#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: user.py
# author: lotus163
import platform
import socket

import psutil
from fastapi import APIRouter, Depends

from backend.core.deps import UserDepends, get_user_depends_nolog
from backend.core.result import ResultData, result_success
from backend.utils.path_util import get_current_directory

router = APIRouter(prefix="/server", tags=["监控"])


@router.get("", response_model=ResultData, summary='获取服务信息')
async def get_server_info(user_depends: UserDepends = Depends(get_user_depends_nolog)):
    """
    {"cpu":{"cpuNum":2,"total":199000.0,"sys":0.5,"used":1.01,"wait":0.0,"free":98.49},
    "mem":{"total":7.56,"used":3.85,"free":3.71,"usage":50.93},
    "jvm":{"total":592.5,"max":1008.0,"free":155.86,"version":"1.8.0_111","home":"/usr/java/jdk1.8.0_111/jre","startTime":"2022-09-26 21:25:14","used":436.64,"runTime":"42天12小时22分钟","inputArgs":"[-Dname=target/ruoyi-vue.jar, -Duser.timezone=Asia/Shanghai, -Xms512m, -Xmx1024m, -XX:MetaspaceSize=128m, -XX:MaxMetaspaceSize=512m, -XX:+HeapDumpOnOutOfMemoryError, -XX:+PrintGCDateStamps, -XX:+PrintGCDetails, -XX:NewRatio=1, -XX:SurvivorRatio=30, -XX:+UseParallelGC, -XX:+UseParallelOldGC]","usage":73.69,"name":"Java HotSpot(TM) 64-Bit Server VM"},
    "sys":{"computerName":"iZwz94p7mycommzd52gme4Z","computerIp":"172.18.179.171","userDir":"/home/ruoyi/projects/ruoyi-vue","osName":"Linux","osArch":"amd64"},
    "sysFiles":[{"dirName":"/","sysTypeName":"ext4","typeName":"/","total":"39.2 GB","free":"25.0 GB","used":"14.2 GB","usage":36.23}]}
    :return:
    """
    cpu = {"cpuNum": psutil.cpu_count(),
           "total": 199000.0,
           "sys": psutil.cpu_times_percent().system,
           "used": psutil.cpu_times_percent().user,
           "wait": psutil.cpu_times_percent().interrupt,
           "free": psutil.cpu_times_percent().idle}
    mem = {"total": psutil.virtual_memory().total,
           "used": psutil.virtual_memory().used,
           "free": psutil.virtual_memory().free,
           "usage": psutil.virtual_memory().percent}
    jvm = {"total": 592.5, "max": 1008.0, "free": 155.86, "version": "1.8.0_111", "home": "/usr/java/jdk1.8.0_111/jre",
           "startTime": "2022-09-26 21:25:14", "used": 436.64, "runTime": "42天12小时22分钟",
           "inputArgs": "[-Dname=target/ruoyi-vue.jar, -Duser.timezone=Asia/Shanghai, -Xms512m, -Xmx1024m, -XX:MetaspaceSize=128m, -XX:MaxMetaspaceSize=512m, -XX:+HeapDumpOnOutOfMemoryError, -XX:+PrintGCDateStamps, -XX:+PrintGCDetails, -XX:NewRatio=1, -XX:SurvivorRatio=30, -XX:+UseParallelGC, -XX:+UseParallelOldGC]",
           "usage": 73.69, "name": "Java HotSpot(TM) 64-Bit Server VM"}
    sys = {"computerName": socket.gethostname(),
           "computerIp": socket.gethostbyname(socket.gethostname()),
           "userDir": get_current_directory(),
           "osName": platform.system(),
           "osArch": platform.machine()}
    sys_files = [{"dirName": "/", "sysTypeName": "ext4", "typeName": "/", "total": "39.2 GB", "free": "25.0 GB",
                  "used": "14.2 GB", "usage": 36.23}]
    return result_success(data={"cpu": cpu, "mem": mem, "jvm": jvm, "sys": sys, "sysFiles": sys_files})
