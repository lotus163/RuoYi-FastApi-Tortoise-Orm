#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: user.py
# author: lotus163
from typing import Optional

from fastapi import APIRouter, Depends

from backend.core.deps import UserDepends, get_user_depends, get_user_depends_nolog
from backend.core.result import Result, result_success, ResultList, result_success_no_log
from backend.crud import OperLogDao
from backend.schemas import OperLogSch
from backend.utils.string_util import get_array_by_str

router = APIRouter(prefix="/operlog", tags=["操作日志"])


@router.delete("/clean", response_model=Result, summary='清除操作日志')
async def clean_oper_logs(user_depends: UserDepends = Depends(get_user_depends)):
    await OperLogDao.get_list_queryset().delete()
    return result_success()


@router.post("/export", response_model=Result, summary='导出操作日志列表')
async def export_oper_logs():
    pass


@router.get("/list", response_model=ResultList[OperLogSch], summary='获取操作日志列表')
async def get_oper_logs(pageNum: int, pageSize: int, title: Optional[str] = None, operName: Optional[str] = None,
                        businessType: Optional[str] = None, status: Optional[str] = None,
                        user_depends: UserDepends = Depends(get_user_depends_nolog)):
    # 准备查询的参数
    form = OperLogSch(title=title, operName=operName, status=status, businessType=businessType)
    form_dict = form.dict(exclude_none=True)
    # 时间
    start, end = user_depends.request_service.get_params_time()
    if (start is not None) and (end is not None):
        form_dict["operTime__range"] = (start, end)
    # 查询
    oper_logs_sch = await OperLogDao.get_list_sch_page(pageSize=pageSize, pageNum=pageNum, **form_dict)
    total = await OperLogDao.get_list_count(**form_dict)
    return result_success_no_log(msg="查询成功", rows=oper_logs_sch, total=total)


@router.delete("/{operIds}", response_model=Result, summary='根据ids删除日志')
async def remove_oper_logs(operIds: str, user_depends: UserDepends = Depends(get_user_depends)):
    open_ids = get_array_by_str(operIds)
    await OperLogDao.get_list_queryset(operId__in=open_ids).delete()
    return result_success()
