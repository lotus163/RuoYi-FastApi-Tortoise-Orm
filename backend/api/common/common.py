#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: sys_user.py
# author: lotus163
from fastapi import APIRouter

from backend.core.result import Result

router = APIRouter(prefix="/common", tags=["通用操作"])


@router.get("/download", response_model=Result, summary='通用下载请求')
async def file_download():
    pass


@router.get("/download/resource", response_model=Result, summary='本地资源通用下载')
async def resource_download():
    pass


@router.post("/upload", response_model=Result, summary='通用上传请求（单个）')
async def upload_file():
    pass


@router.post("/uploads", response_model=Result, summary='通用上传请求（多个）')
async def upload_files():
    pass
