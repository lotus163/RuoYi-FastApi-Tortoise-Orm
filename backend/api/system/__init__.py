#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: login.py
# author: lotus163
from fastapi import APIRouter

from backend.api.system import sys_user, role, post, notice, menu, dict_type, dict_data, config, dept

router = APIRouter(prefix="/system")
# 包含子路由
router.include_router(sys_user.router)
router.include_router(role.router)
router.include_router(post.router)
router.include_router(notice.router)
router.include_router(menu.router)
router.include_router(dict_type.router)
router.include_router(dict_data.router)
router.include_router(dept.router)
router.include_router(config.router)
