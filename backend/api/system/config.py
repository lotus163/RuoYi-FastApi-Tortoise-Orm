#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: user.py
# author: lotus163
from typing import Optional

from fastapi import APIRouter, Depends

from backend.core.deps import UserDepends, get_user_depends
from backend.core.result import Result, result_success, ResultData, ResultList
from backend.crud import SysConfigDao
from backend.schemas import SysConfigAdd, SysConfigSch
from backend.service.system.sys_config import SysConfigService
from backend.utils.form_util import get_form_dict
from backend.utils.string_util import get_array_by_str

router = APIRouter(prefix="/config", tags=["参数"])


@router.put("", response_model=Result, summary='修改参数')
async def update_config(form: SysConfigSch, user_depends: UserDepends = Depends(get_user_depends)):
    config_db = await SysConfigDao.get_db_or_none(configId=form.configId)
    # 添加创建者
    form.updateBy = user_depends.login_user.user.userName
    await SysConfigDao.update_sch(config_db, **form.dict(exclude_none=True))
    return result_success()


@router.post("", response_model=Result, summary='新增参数')
async def add_config(form: SysConfigAdd, user_depends: UserDepends = Depends(get_user_depends)):
    form.createBy = user_depends.login_user.user.userName
    await SysConfigDao.add_sch(**form.dict(exclude_none=True))
    return result_success()


@router.post("/export", response_model=Result, summary='导出参数列表')
async def export_configs_list():
    pass


@router.get("/list", response_model=ResultList[SysConfigSch], summary='获取参数列表')
async def get_configs_list(pageNum: int, pageSize: int,
                           configName: Optional[str] = None,
                           configKey: Optional[str] = None,
                           configType: Optional[str] = None,
                           user_depends: UserDepends = Depends(get_user_depends)):
    # 时间
    start, end = user_depends.request_service.get_params_time()
    # 准备查询的参数
    form_dict = get_form_dict(form=SysConfigSch(configName=configName, configKey=configKey, configType=configType),
                              start=start, end=end, time_range="createTime__range")
    # 查询
    users_sch = await SysConfigDao.get_list_sch_page(pageSize=pageSize, pageNum=pageNum, **form_dict)
    total = await SysConfigDao.get_list_count(**form_dict)
    return result_success(msg="查询成功", rows=users_sch, total=total)


@router.delete("/refreshCache", response_model=Result, summary='刷新参数缓存')
async def refresh_config_cache(user_depends: UserDepends = Depends(get_user_depends)):
    await user_depends.redis.loading_config_cache()
    return result_success()


@router.delete("/{configIds}", response_model=Result, summary='根据ids删除参数')
async def remove_configs(configIds: str, user_depends: UserDepends = Depends(get_user_depends)):
    await SysConfigDao.delete_db(configId__in=get_array_by_str(configIds))
    return result_success()


@router.get("/{configId}", response_model=ResultData, summary='根据id获取参数详情')
async def get_config_info(configId: int, user_depends: UserDepends = Depends(get_user_depends)):
    return result_success(data=await SysConfigDao.get_sch_or_none(configId=configId))
