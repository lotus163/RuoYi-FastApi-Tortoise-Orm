#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: user.py
# author: lotus163
from typing import Optional

from fastapi import APIRouter, Depends

from ...core.datascope import DataScope
from ...core.deps import UserDepends, get_user_depends
from ...core.result import Result, result_success, ResultData, ResultMenuTree
from ...crud import MenuDao
from ...schemas import SysMenuAdd, SysMenuSch
from ...service.system.menu import MenuService
from ...service.system.role_menu import RoleMenuService
from ...utils.form_util import get_form_dict

router = APIRouter(prefix="/menu", tags=["菜单"])


@router.put("", response_model=Result, summary='修改菜单')
async def update_menu(form: SysMenuSch, user_depends: UserDepends = Depends(get_user_depends)):
    menu_db = await MenuDao.get_db_or_none(menuId=form.menuId)
    # 数据过滤,失败则抛出异常
    if not user_depends.login_user.user.admin:
        DataScope(user_depends.login_user.data_scope.roles_menus_ids).has_sope(form.menuId)
        # 添加创建者
    form.updateBy = user_depends.login_user.user.userName
    await MenuDao.update_sch(obj=menu_db, **form.dict(exclude_none=True))
    return result_success()


@router.post("", response_model=Result, summary='新增菜单')
async def add_menu(form: SysMenuAdd, user_depends: UserDepends = Depends(get_user_depends)):
    form.createBy = user_depends.login_user.user.userName
    await MenuDao.add_sch(**form.dict(exclude_none=True))
    return result_success()


@router.get("/list", response_model=ResultData, summary='获取菜单列表')
async def get_menus_list(menuName: Optional[str] = None, status: Optional[str] = None,
                         user_depends: UserDepends = Depends(get_user_depends)):
    form_dict = get_form_dict(form=SysMenuAdd(menuName=menuName, status=status))
    if not user_depends.login_user.user.admin:
        form_dict["menuId__in"] = user_depends.login_user.data_scope.roles_menus_ids
    menus_sch = await MenuDao.get_list_sch(**form_dict)
    return result_success(data=menus_sch)


@router.get("/roleMenuTreeselect/{roleId}", response_model=ResultMenuTree, summary='加载对应角色菜单列表树')
async def get_role_menu_tree_select(roleId: int, user_depends: UserDepends = Depends(get_user_depends)):
    menus_ids = await RoleMenuService().get_menu_ids_by_role_id(roleId)
    menus_sch = await MenuDao.get_list_sch()
    menu_tree_schs = await MenuService().get_menus_tree(menus_sch)
    return result_success(checkedKeys=menus_ids, menus=menu_tree_schs)


@router.get("/treeselect", response_model=ResultData, summary='获取菜单下拉树列表')
async def get_menus_tree_elect(user_depends: UserDepends = Depends(get_user_depends)):
    menus_sch = await MenuDao.get_list_sch()
    menu_tree_schs = await MenuService().get_menus_tree(menus_sch)
    return result_success(data=menu_tree_schs)


@router.get("/{menuId}", response_model=ResultData, summary='根据id获取菜单信息')
async def get_menu_info(menuId: int, user_depends: UserDepends = Depends(get_user_depends)):
    if not user_depends.login_user.user.admin:
        DataScope(user_depends.login_user.data_scope.roles_menus_ids).has_sope(menuId)
    menu_sch = await MenuDao.get_sch_or_none(menuId=menuId)
    return result_success(data=menu_sch)


@router.delete("/{menuId}", response_model=Result, summary='根据id删除菜单')
async def remove_menu(menuId: int, user_depends: UserDepends = Depends(get_user_depends)):
    if not user_depends.login_user.user.admin:
        DataScope(user_depends.login_user.data_scope.roles_menus_ids).has_sope(menuId)
    await MenuDao.delete_db(menuId=menuId)
    return result_success()
