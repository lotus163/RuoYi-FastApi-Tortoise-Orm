#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: user.py
# author: lotus163
from typing import Optional

from fastapi import APIRouter, Depends

from backend.core.deps import UserDepends, get_user_depends
from backend.core.result import Result, result_success, ResultData, ResultList
from backend.crud import DictDataDao
from backend.schemas import SysDictDataAdd, SysDictDataSch
from backend.utils.form_util import get_form_dict
from backend.utils.string_util import get_array_by_str

router = APIRouter(prefix="/dict/data", tags=["字典"])


@router.put("", response_model=Result, summary='修改字典数据')
async def update_dict_data(form: SysDictDataSch, user_depends: UserDepends = Depends(get_user_depends)):
    dict_data_db = await DictDataDao.get_db_or_none(dictCode=form.dictCode)
    # 添加创建者
    form.updateBy = user_depends.login_user.user.userName
    await DictDataDao.update_sch(obj=dict_data_db, **form.dict(exclude_none=True))
    return result_success()


@router.post("", response_model=Result, summary='新增字典数据')
async def add_dict_data(form: SysDictDataAdd, user_depends: UserDepends = Depends(get_user_depends)):
    form.createBy = user_depends.login_user.user.userName
    await DictDataDao.add_sch(**form.dict(exclude_none=True))
    return result_success()


@router.post("/export", response_model=Result, summary='导出字典数据')
async def export_dict_data():
    pass


@router.get("/list", response_model=ResultList[SysDictDataSch], summary='获取字典信息列表')
async def get_dict_datas_list(pageNum: int, pageSize: int,
                              dictType: Optional[str] = None,
                              dictLabel: Optional[str] = None,
                              status: Optional[str] = None,
                              user_depends: UserDepends = Depends(get_user_depends)):
    start, end = user_depends.request_service.get_params_time()
    # 准备查询的参数
    form_dict = get_form_dict(form=SysDictDataSch(dictType=dictType, dictLabel=dictLabel, status=status),
                              start=start, end=end, time_range="createTime__range")
    # 查询
    dict_datas_sch = await DictDataDao.get_list_sch_page(pageSize=pageSize, pageNum=pageNum, **form_dict)
    total = await DictDataDao.get_list_count(**form_dict)
    return result_success(msg="查询成功", rows=dict_datas_sch, total=total)


@router.get("/type/{dictType}", response_model=ResultData, summary='根据type查询字典data列表')
async def get_dict_type(dictType: str, user_depends: UserDepends = Depends(get_user_depends)):
    return result_success(data=await user_depends.redis.get_dict_cache(dictType))


@router.delete("/{dictCodes}", response_model=Result, summary='根据ids删除字典数据')
async def remove_dict_datas(dictCodes: str):
    await DictDataDao.delete_db(dictCode__in=get_array_by_str(dictCodes))
    return result_success()


@router.get("/{dictCode}", response_model=ResultData, summary='查询字典数据详细')
async def get_dict_data_info(dictCode: int, user_depends: UserDepends = Depends(get_user_depends)):
    return result_success(data=await DictDataDao.get_sch_or_none(dictCode=dictCode))
