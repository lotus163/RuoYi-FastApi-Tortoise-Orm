#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/19 15:11
# file: user.py
# author: lotus163
from typing import Optional

from fastapi import APIRouter, Depends

from backend.core.deps import UserDepends, get_user_depends
from backend.core.result import Result, result_success, ResultList, ResultData
from backend.crud import DictTypeDao
from backend.schemas import SysDictTypeAdd, SysDictTypeSch
from backend.utils.form_util import get_form_dict
from backend.utils.string_util import get_array_by_str

router = APIRouter(prefix="/dict/type", tags=["字典"])


@router.put("", response_model=Result, summary='修改字典类型')
async def update_dict_type(form: SysDictTypeSch, user_depends: UserDepends = Depends(get_user_depends)):
    dict_type_db = await DictTypeDao.get_db_or_none(dictId=form.dictId)
    form.updateBy = user_depends.login_user.user.userName
    await DictTypeDao.update_sch(obj=dict_type_db, **form.dict(exclude_none=True))
    return result_success()


@router.post("", response_model=Result, summary='新增字典类型')
async def add_dict_type(form: SysDictTypeAdd, user_depends: UserDepends = Depends(get_user_depends)):
    form.createBy = user_depends.login_user.user.userName
    await DictTypeDao.add_sch(**form.dict(exclude_none=True))
    return result_success()


@router.post("/export", response_model=Result, summary='导出所有字典')
async def export_dicts():
    pass


@router.get("/list", response_model=ResultList[SysDictTypeSch], summary='获取所有字典类型列表')
async def get_dict_type_list(pageNum: int, pageSize: int,
                             dictName: Optional[str] = None,
                             dictType: Optional[str] = None,
                             status: Optional[str] = None,
                             user_depends: UserDepends = Depends(get_user_depends)):
    start, end = user_depends.request_service.get_params_time()
    # 准备查询的参数
    form_dict = get_form_dict(form=SysDictTypeSch(dictName=dictName, dictType=dictType, status=status),
                              start=start, end=end, time_range="createTime__range")
    # 查询
    dict_types_sch = await DictTypeDao.get_list_sch_page(pageSize=pageSize, pageNum=pageNum, **form_dict)
    total = await DictTypeDao.get_list_count(**form_dict)
    return result_success(msg="查询成功", rows=dict_types_sch, total=total)


@router.get("/optionselect", response_model=ResultData, summary='获取字典选择框列表')
async def get_dict_type_option_select(user_depends: UserDepends = Depends(get_user_depends)):
    return result_success(data=await DictTypeDao.get_list_sch())


@router.delete("/refreshCache", response_model=Result, summary='刷新字典缓存')
async def refresh_dict_cache(user_depends: UserDepends = Depends(get_user_depends)):
    await user_depends.redis.loading_dict_cache()
    return result_success()


@router.delete("/{dictIds}", response_model=Result, summary='删除字典类型')
async def remove_dict_types(dictIds: str, user_depends: UserDepends = Depends(get_user_depends)):
    await DictTypeDao.delete_db(dictId__in=get_array_by_str(dictIds))
    return result_success()


@router.get("/{dictId}", response_model=ResultData, summary='查询字典类型详细')
async def get_dict_info(dictId: int, user_depends: UserDepends = Depends(get_user_depends)):
    return result_success(data=await DictTypeDao.get_sch_or_none(dictId=dictId))
