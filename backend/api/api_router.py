#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/21 9:40
# file: api_router.py
# author: lotus163

from fastapi import APIRouter

from backend.api import system, monitor
from backend.api.common import common
from backend.api.system import login
from backend.config.setting import settings
from backend.core.result import Result

router = APIRouter(prefix=settings.API_PREFIX)
router.include_router(login.router)
router.include_router(system.router)
router.include_router(monitor.router)
router.include_router(common.router)


@router.get("/", response_model=Result, summary='访问首页，提示语')
async def get_index():
    return "欢迎使用。"
