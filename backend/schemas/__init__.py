#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/15 12:30
# file: __init__.py.py
# author: lotus163
from .base import BaseSch
from .monitor.login_infor import LoginInforSch
from .monitor.oper_log import OperLogSch
from .monitor.user_online import UserOnlineSch
from .system.dept import SysDeptSch, SysDeptAdd, SysDeptTreeSch
from .system.login import LoginForm, LoginUserSch
from .system.menu import SysMenuSch, RouterMetaSch, SysRouterSch, SysMenuAdd, SysMenuTreeSch
from .system.notice import SysNoticeSch, SysNoticeAdd
from .system.post import SysPostSch, SysPostAdd
from .system.role import SysRoleSch, SysRoleAdd, ChangeRoleStatusForm, SysRoleDeptSch, CancelUserRoleForm, \
    CancelAllUserRoleForm
from .system.sys_config import SysConfigSch, SysConfigAdd
from .system.sys_dict import SysDictDataSch, SysDictTypeSch, SysDictDataAdd, SysDictTypeAdd,RedisDictDataSch
from .system.token import Token
from .system.user import SysUserSch, SysUserAdd, ChangeUserStatusForm, ResetPwdForm,UserAvatarForm
from .system.scope import DataScopeSch
