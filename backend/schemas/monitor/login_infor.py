#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File: login_infor.py
# @Author: lotus163
# @Date: 2022/11/24
from datetime import datetime
from typing import Optional

import orjson
from pydantic import BaseModel, validator


class LoginInforSch(BaseModel):
    infoId: Optional[int] = None
    userName: Optional[str] = None
    ipaddr: Optional[str] = None
    loginLocation: Optional[str] = None
    browser: Optional[str] = None
    os: Optional[str] = None
    status: Optional[str] = "0"
    msg: Optional[str] = "正常登录"
    loginTime: Optional[datetime] = None

    @validator("loginTime")
    def format_time(cls, value: datetime) -> str:
        return value.strftime('%Y-%m-%d %H:%M:%S')  # 格式化时间

    class Config:
        orm_mode = True
        json_loads = orjson.loads
        json_dumps = orjson.dumps
