#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/23 9:40
# file: user_online.py
# author: lotus163
from datetime import datetime
from typing import Optional

import orjson
from pydantic import BaseModel, validator


class UserOnlineSch(BaseModel):
    ipaddr: Optional[str] = None
    loginLocation: Optional[str] = None
    browser: Optional[str] = None
    os: Optional[str] = None
    deptName: Optional[str] = None
    loginTime: Optional[datetime] = None
    userName: Optional[str] = None
    tokenId: Optional[str] = None  # 部门ID

    @validator("loginTime")
    def format_time(cls, value: datetime) -> str:
        return value.strftime('%Y-%m-%d %H:%M:%S')  # 格式化时间

    class Config:
        orm_mode = True
        json_loads = orjson.loads
        json_dumps = orjson.dumps
