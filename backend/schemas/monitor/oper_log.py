#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File: oper_log.py
# @Author: lotus163
# @Date: 2022/11/22
from datetime import datetime
from typing import Optional

import orjson
from pydantic import BaseModel, validator


class OperLogSch(BaseModel):
    operId: Optional[int] = None
    title: Optional[str] = None
    businessType: Optional[str] = None
    method: Optional[str] = None
    requestMethod: Optional[str] = None
    operatorType: Optional[str] = None
    operName: Optional[str] = None
    deptName: Optional[str] = None
    operUrl: Optional[str] = None
    operIp: Optional[str] = None
    operLocation: Optional[str] = None
    operParam: Optional[str] = None
    jsonResult: Optional[str] = None
    status: Optional[str] = None
    errorMsg: Optional[str] = None
    operTime: Optional[datetime] = None

    @validator("operTime")
    def format_time(cls, value: datetime) -> str:
        return value.strftime('%Y-%m-%d %H:%M:%S')  # 格式化时间

    class Config:
        orm_mode = True
        json_loads = orjson.loads
        json_dumps = orjson.dumps
