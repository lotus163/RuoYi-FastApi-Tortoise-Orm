#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/28 16:22
# file: role.py
# author: lotus163
from typing import Optional, List, Set

from pydantic import BaseModel

from backend.schemas.base import BaseSch


class SysRoleAdd(BaseSch):
    roleName: Optional[str] = None  # 角色名称
    roleKey: Optional[str] = None  # 角色权限
    roleSort: Optional[str] = None  # 角色排序
    status: Optional[str] = None  # 角色状态,0=正常,1=停用
    deptCheckStrictly: Optional[bool] = None  # 部门树选择项是否关联显示（0：父子不互相关联显示 1：父子互相关联显示 ）
    menuCheckStrictly: Optional[bool] = None  # 菜单树选择项是否关联显示（ 0：父子不互相关联显示 1：父子互相关联显示）
    menuIds: Optional[List] = None  # 菜单组
    deptIds: Optional[List] = None  # 部门组（数据权限）


class SysRoleSch(SysRoleAdd):
    roleId: Optional[int] = None  # 角色ID
    dataScope: Optional[str] = None  # 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限；5：仅本人数据权限）
    delFlag: Optional[str] = None  # 删除标志（0代表存在 2代表删除）
    flag: Optional[bool] = None  # 用户是否存在此角色标识 默认不存在
    permissions: Optional[Set] = None  # 角色排序


class ChangeRoleStatusForm(BaseModel):
    status: Optional[str] = None
    roleId: Optional[int] = None


class SysRoleDeptSch(BaseModel):
    id: Optional[int] = None
    label: Optional[str] = None
    children: List = []


class CancelUserRoleForm(BaseModel):
    userId: Optional[int] = None
    roleId: Optional[int] = None


class CancelAllUserRoleForm(BaseModel):
    userIds: Optional[List[int]] = None
    roleId: Optional[int] = None
