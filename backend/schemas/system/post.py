#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/16 12:11
# file: post.py
# author: lotus163
from typing import Optional

from backend.schemas import BaseSch


class SysPostAdd(BaseSch):
    postCode: Optional[str] = None
    postName: Optional[str] = None
    postSort: Optional[str] = None
    status: Optional[str] = None


class SysPostSch(SysPostAdd):
    postId: Optional[int] = None
