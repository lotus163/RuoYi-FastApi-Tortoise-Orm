#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File: sys_dict.py
# @Author: lotus163
# @Date: 2022/10/30
from typing import Optional

from pydantic import BaseModel

from backend.schemas.base import BaseSch


class SysDictDataAdd(BaseSch):
    cssClass: Optional[str] = None  # 样式属性（其他样式扩展）
    dictLabel: Optional[str] = None  # 字典标签
    dictSort: Optional[int] = None  # 字典排序
    dictType: Optional[str] = None  # 字典类型
    dictValue: Optional[str] = None  # 字典键值
    listClass: Optional[str] = None  # 表格字典样式
    status: Optional[str] = None  # 状态（0正常 1停用）


class SysDictDataSch(SysDictDataAdd):
    dictCode: Optional[int] = None  # 字典编码
    isDefault: Optional[str] = None  # 是否默认（Y是 N否）


class SysDictTypeAdd(BaseSch):
    dictName: Optional[str] = None  # 字典名称
    dictType: Optional[str] = None  # 字典类型
    status: Optional[str] = None  # 状态（0正常 1停用）


class SysDictTypeSch(SysDictTypeAdd):
    dictId: Optional[int] = None  # 字典主键


class RedisDictDataSch(BaseModel):
    """为了列表"""
    __root__: list[SysDictDataSch]
