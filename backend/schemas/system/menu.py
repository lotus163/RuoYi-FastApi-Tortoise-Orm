#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/28 16:28
# file: menu.py
# author: lotus163
from typing import Optional, List

from pydantic import BaseModel

from backend.schemas.base import BaseSch


class SysMenuAdd(BaseSch):
    component: Optional[str] = None  # 组件路径
    isCache: Optional[str] = None  # 是否缓存（0缓存 1不缓存）
    isFrame: Optional[str] = None  # 是否为外链（0是 1否）
    menuName: Optional[str] = None  # 菜单名称
    menuType: Optional[str] = None  # 类型（M目录 C菜单 F按钮）
    orderNum: Optional[int] = None  # 显示顺序
    parentId: Optional[int] = None  # 父菜单ID
    path: Optional[str] = None  # 路由地址
    perms: Optional[str] = None  # 权限字符串
    query: Optional[str] = None  # 路由参数
    visible: Optional[str] = None  # 显示状态（0显示 1隐藏）
    status: Optional[str] = None  # 菜单状态（0显示 1隐藏）
    icon: Optional[str] = None  # 菜单图标


class SysMenuSch(SysMenuAdd):
    menuId: Optional[int] = None  # 菜单ID
    parentName: Optional[str] = None  # 父菜单名称
    children: Optional[List] = []  # 子菜单


class RouterMetaSch(BaseModel):
    icon: Optional[str] = None
    link: Optional[str] = None
    noCache: Optional[bool] = None
    title: Optional[str] = None


class SysRouterSch(BaseModel):
    """
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * roles: ['admin', 'common']       // 访问路由的角色权限
 * permissions: ['a:a:a', 'b:b:b']  // 访问路由的菜单权限
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
    """
    alwaysShow: Optional[bool] = None
    children: Optional[List] = []
    component: Optional[str] = None
    hidden: Optional[bool] = None
    meta: Optional[RouterMetaSch] = None
    name: Optional[str] = None
    path: Optional[str] = None
    redirect: Optional[str] = None


class SysMenuTreeSch(BaseModel):
    id: Optional[int] = None
    label: Optional[str] = None
    children: Optional[List] = []
