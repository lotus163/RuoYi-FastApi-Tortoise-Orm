#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/16 11:45
# file: notice.py
# author: lotus163
from typing import Optional

from backend.schemas import BaseSch


class SysNoticeAdd(BaseSch):
    noticeTitle: Optional[str] = None
    noticeType: Optional[str] = None
    noticeContent: Optional[str] = None
    status: Optional[str] = None


class SysNoticeSch(SysNoticeAdd):
    noticeId: Optional[int] = None  # 主键
