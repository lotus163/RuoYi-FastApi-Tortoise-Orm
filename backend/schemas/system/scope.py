#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/28 19:58
# file: scope.py
# author: lotus163
from typing import Optional, List

import orjson
from pydantic import BaseModel


class DataScopeSch(BaseModel):
    user_id:Optional[int] = None
    admin:Optional[bool] = None
    user_dept_id:Optional[int] = None # 所属部门id
    user_roles_ids:Optional[List[int]] = None  # 用户的角色Ids
    user_posts_ids:Optional[List[int]] = None  # 角色的岗位postIds
    roles_depts_ids:Optional[List[int]] = None  # 角色的部门Ids
    roles_menus_ids:Optional[List[int]] = None  # 角色的部门Ids
    roles_depts_users_ids:Optional[List[int]] = None  # 角色的部门Ids
    permissions: Optional[List[str]] = None  # 权限列表


    class Config:
        json_loads = orjson.loads
        json_dumps = orjson.dumps
