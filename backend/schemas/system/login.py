from typing import Optional, List

from pydantic import BaseModel

from backend.core.datascope import DataScope
from backend.schemas.monitor.login_infor import LoginInforSch
from backend.schemas.system.scope import DataScopeSch
from backend.schemas.system.user import SysUserSch


class LoginForm(BaseModel):
    """LoginBody"""
    code: Optional[str] = None
    password: Optional[str] = None
    username: Optional[str] = None
    uuid: Optional[str] = None


class LoginUserSch(BaseModel):
    login_infor: Optional[LoginInforSch] = None
    user: Optional[SysUserSch] = None  # 用户信息
    data_scope:Optional[DataScopeSch] = None # 权限

    token: Optional[str] = None  # 用户唯一标识
    uuid: Optional[str] = None  # 用户唯一标识
    expireTime: Optional[int] = None  # 过期时间
    userId:Optional[int] = None
