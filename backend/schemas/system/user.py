#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/28 16:09
# file: sys_user.py
# author: lotus163
from datetime import datetime
from typing import List, Optional

from pydantic import EmailStr, BaseModel

from backend.schemas.base import BaseSch
from backend.schemas.system.dept import SysDeptSch
from backend.schemas.system.role import SysRoleSch


class SysUserAdd(BaseSch):
    deptId: Optional[int] = None  # 部门ID
    email: Optional[str] = None  # 用户邮箱
    phonenumber: Optional[str] = None  # 手机号码
    sex: Optional[str] = None  # 用户性别,0=男,1=女,2=未知
    userName: Optional[str] = None  # 用户账号
    nickName: Optional[str] = None  # 用户昵称
    status: Optional[str] = None  # 帐号状态,0=正常,1=停用
    password: Optional[str] = None  # 密码
    postIds: Optional[List[int]] = None  # 岗位组
    roleIds: Optional[List[int]] = None  # 角色组


class SysUserSch(SysUserAdd):
    userId: Optional[int] = None  # 用户ID
    avatar: Optional[str] = None  # 用户头像
    delFlag: Optional[str] = None  # 删除标志（0代表存在 2代表删除）
    loginIp: Optional[str] = None  # 最后登录IP
    loginDate: Optional[datetime] = None  # 最后登录时间
    dept: Optional[SysDeptSch] = None  # 部门对象
    roles: Optional[List[SysRoleSch]] = None  # 角色对象
    admin: Optional[bool] = None

    class Config:
        fields = {'password': {'exclude': True}}


class ChangeUserStatusForm(BaseModel):
    status: Optional[str] = None
    userId: Optional[int] = None


class ResetPwdForm(BaseModel):
    password: Optional[str] = None
    userId: Optional[int] = None


class UserAvatarForm(BaseModel):
    avatarfile: Optional[bytes]= None

# class UserProfileForm(BaseModel):
#     admin: true
#     avatar: "/profile/avatar/2022/08/10/blob_20220810105007A010.jpeg"
#     createBy: "admin"
#     createTime: "2022-05-18 09:25:24"
#     delFlag: "0"
#     dept: Object
#     deptId: 103
#     email: "1971679054@qq.com"
#     loginDate: "2022-08-26T09:15:17.000+08:00"
#     loginIp: "127.0.0.1"
#     nickName: "aaaa"
#     params: Object
#     phonenumber: "15888888888"
#     postIds: null
#     remark: "管理员"
#     roleId: null
#     roleIds: null
#     roles: Array(1)
#     salt: null
#     searchValue: null
#     sex: "1"
#     status: "0"
#     updateBy: null
#     updateTime: null
#     userId: 1
#     userName: "admin"
