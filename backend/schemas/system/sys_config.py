#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/8 10:22
# file: config.py
# author: lotus163
from typing import Optional

from backend.schemas.base import BaseSch


class SysConfigAdd(BaseSch):
    configName: Optional[str] = None  # 部门ID
    configKey: Optional[str] = None  # 用户账号
    configValue: Optional[str] = None
    configType: Optional[str] = None


class SysConfigSch(SysConfigAdd):
    configId: Optional[int] = None
