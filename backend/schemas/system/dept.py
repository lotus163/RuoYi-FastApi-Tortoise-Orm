#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/28 16:28
# file: dept.py
# author: lotus163
from typing import Optional, List

from pydantic import BaseModel

from backend.schemas.base import BaseSch


class SysDeptAdd(BaseSch):
    deptName: Optional[str] = None  # 显示顺序
    email: Optional[str] = None  # 邮箱
    leader: Optional[str] = None  # 负责人
    orderNum: Optional[int] = None  # 父部门ID
    phone: Optional[str] = None  # 联系电话
    parentId: Optional[int] = None  # 父部门ID
    status: Optional[str] = None  # 部门状态:0正常,1停用


class SysDeptSch(SysDeptAdd):
    deptId: Optional[int] = None  # 部门ID
    ancestors: Optional[str] = None  # 祖级列表
    delFlag: Optional[str] = None  # 删除标志（0代表存在 2代表删除）
    parentName: Optional[str] = None  # 父部门名称
    children: Optional[List] = None  # 子部门


class SysDeptTreeSch(BaseModel):
    id: Optional[int] = None
    label: Optional[str] = None
    children: Optional[List] = []
