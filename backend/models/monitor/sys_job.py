#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 16:16
# file: sys_job.py
# author: lotus163

from tortoise import fields
from tortoise.models import Model

from backend.models.base import BaseTimeModel


class SysJobDb(BaseTimeModel):
    jobId = fields.BigIntField(source_field="job_id", pk=True, describe="任务ID")
    jobName = fields.CharField(source_field="job_name", max_length=64, null=True, default="", describe="任务名称")
    jobGroup = fields.CharField(source_field="job_group", max_length=64, null=True, default="DEFAULT",
                                describe="任务组名")
    invokeTarget = fields.CharField(source_field="invoke_target", max_length=500, describe="调用目标字符串")
    cronExpression = fields.CharField(source_field="cron_expression", max_length=255, null=True, default="",
                                      describe="cron执行表达式")
    misfirePolicy = fields.CharField(source_field="misfire_policy", max_length=20, null=True, default="3",
                                     describe="计划执行错误策略（1立即执行 2执行一次 3放弃执行）")
    concurrent = fields.CharField(max_length=1, null=True, default="1", describe="是否并发执行（0允许 1禁止）")
    status = fields.CharField(max_length=1, null=True, default="0", describe="状态（0正常 1暂停）")
    remark = fields.CharField(max_length=500, null=True, default="", describe="备注")

    class Meta:
        table = "sys_job"
        table_description = "定时任务调度表"


class SysJobLogDb(Model):
    jobLogId = fields.BigIntField(source_field="job_log_id", pk=True, describe="任务日志ID")
    jobName = fields.CharField(source_field="job_name", max_length=64, describe="任务名称")
    jobGroup = fields.CharField(source_field="job_group", max_length=64, describe="任务组名")
    invokeTarget = fields.CharField(source_field="invoke_target", max_length=500, describe="调用目标字符串")
    jobMessage = fields.CharField(source_field="job_message", max_length=500, null=True, describe="日志信息")
    status = fields.CharField(max_length=1, null=True, default="0", describe="执行状态（0正常 1失败）")
    exceptionInfo = fields.CharField(source_field="exception_info", max_length=2000, null=True, default="",
                                     describe="异常信息")
    createTime = fields.DatetimeField(source_field="create_time", auto_now_add=True, null=True, describe="创建时间")

    class Meta:
        table = "sys_job_log"
        table_description = "定时任务调度日志表"
