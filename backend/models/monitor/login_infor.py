#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 16:42
# file: login_infor.py
# author: lotus163

from tortoise import fields
from tortoise.models import Model


class LoginInforDb(Model):
    infoId = fields.BigIntField(source_field="info_id", pk=True, describe="访问ID")
    userName = fields.CharField(source_field="user_name", max_length=50, null=True, default="", describe="用户账号")
    ipaddr = fields.CharField(max_length=128, null=True, default="", describe="登录IP地址")
    loginLocation = fields.CharField(source_field="login_location", max_length=255, null=True, default="",
                                     describe="登录地点")
    browser = fields.CharField(max_length=50, null=True, default="", describe="浏览器类型")
    os = fields.CharField(max_length=50, null=True, default="", describe="操作系统")
    status = fields.CharField(max_length=1, null=True, default="0", describe="登录状态（0成功 1失败）")
    msg = fields.CharField(max_length=255, null=True, default="", describe="提示消息")
    loginTime = fields.DatetimeField(source_field="login_time", auto_now_add=True, null=True, describe="访问时间")

    class Meta:
        table = "sys_logininfor"
        table_description = "系统访问记录"
