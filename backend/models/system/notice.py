#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 15:51
# file: notice.py
# author: lotus163

from tortoise import fields

from backend.models.base import BaseTimeModel


class SysNoticeDb(BaseTimeModel):
    noticeId = fields.BigIntField(source_field="notice_id", pk=True, describe="公告ID")
    noticeTitle = fields.CharField(source_field="notice_title", max_length=50, describe="公告标题")
    noticeType = fields.CharField(source_field="notice_type", max_length=1, describe="公告类型（1通知 2公告）")
    noticeContent = fields.TextField(source_field="notice_content", null=True, describe="公告内容")
    status = fields.CharField(max_length=1, null=True, default="0", describe="公告状态（0正常 1关闭）")
    remark = fields.CharField(max_length=255, null=True, describe="备注")

    class Meta:
        table = "sys_notice"
        table_description = "通知公告表"
