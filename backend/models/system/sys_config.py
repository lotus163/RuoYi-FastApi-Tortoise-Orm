#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 16:03
# file: sys_config.py
# author: lotus163


from tortoise import fields

from backend.models.base import BaseTimeModel


class SysConfigDb(BaseTimeModel):
    configId = fields.IntField(source_field="config_id", pk=True, describe="参数主键")
    configName = fields.CharField(source_field="config_name", max_length=100, null=True, default="",
                                  describe="参数名称")
    configKey = fields.CharField(source_field="config_key", max_length=100, null=True, default="",
                                 describe="参数键名")
    configValue = fields.CharField(source_field="config_value", max_length=500, null=True, default="",
                                   describe="参数键值")
    configType = fields.CharField(source_field="config_type", max_length=1, null=True, default="N",
                                  describe="系统内置（Y是 N否）")
    remark = fields.CharField(max_length=500, null=True, describe="备注")

    class Meta:
        table = "sys_config"
        table_description = "参数配置表"
