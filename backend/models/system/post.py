#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 15:59
# file: post.py
# author: lotus163

from tortoise import fields

from backend.models.base import BaseTimeModel


class SysPostDb(BaseTimeModel):
    postId = fields.BigIntField(source_field="post_id", pk=True, describe="岗位ID")
    postCode = fields.CharField(source_field="post_code", max_length=64, describe="岗位编码")
    postName = fields.CharField(source_field="post_name", max_length=50, describe="岗位名称")
    postSort = fields.IntField(source_field="post_sort", describe="显示顺序")
    status = fields.CharField(max_length=1, describe="状态（0正常 1停用）")
    remark = fields.CharField(max_length=500, null=True, describe="备注")

    class Meta:
        table = "sys_post"
        table_description = "岗位信息表"
