#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 15:04
# file: role.py
# author: lotus163
from tortoise import fields
from tortoise.models import Model

from backend.models.base import BaseTimeModel


class SysRoleDb(BaseTimeModel):
    roleId = fields.BigIntField(source_field="role_id", pk=True, describe="角色ID")
    roleName = fields.CharField(source_field="role_name", max_length=30, describe="角色名称")
    roleKey = fields.CharField(source_field="role_key", max_length=100, describe="角色权限字符串")
    roleSort = fields.IntField(source_field="role_sort", describe="显示顺序")
    dataScope = fields.CharField(source_field="data_scope", max_length=1, null=True, default="1",
                                 describe="数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）")
    menuCheckStrictly = fields.IntField(source_field="menu_check_strictly", null=True, default=1,
                                        describe="菜单树选择项是否关联显示")
    deptCheckStrictly = fields.IntField(source_field="dept_check_strictly", null=True, default=1,
                                        describe="部门树选择项是否关联显示")
    status = fields.CharField(max_length=1, describe="角色状态（0正常 1停用）")
    delFlag = fields.CharField(source_field="del_flag", max_length=1, null=True, default="0",
                               describe="删除标志（0代表存在 2代表删除）")
    remark = fields.CharField(max_length=500, null=True, describe="备注")

    class Meta:
        table = "sys_role"
        table_description = "角色信息表"


class SysRoleDeptDb(Model):
    roleId = fields.BigIntField(source_field="role_id", describe="角色ID")
    deptId = fields.BigIntField(source_field="dept_id", describe="部门ID")

    class Meta:
        table = 'sys_role_dept'
        table_description = "角色和部门关联表"


class SysRoleMenuDb(Model):
    roleId = fields.BigIntField(source_field="role_id", describe="角色ID")
    menuId = fields.BigIntField(source_field="menu_id", describe="菜单ID")

    class Meta:
        table = 'sys_role_menu'
        table_description = "角色和菜单关联表"
