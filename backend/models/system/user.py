#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 13:04
# file: user.py
# author: lotus163

from tortoise import fields
from tortoise.models import Model

from backend.models.base import BaseTimeModel


class SysUserDb(BaseTimeModel):
    userId = fields.BigIntField(source_field="user_id", pk=True, describe="用户ID")
    deptId = fields.BigIntField(source_field="dept_id", null=True, describe="用户ID")
    userName = fields.CharField(source_field="user_name", max_length=30, unique=True, describe="用户账号")
    nickName = fields.CharField(source_field="nick_name", max_length=30, describe="用户昵称")
    userType = fields.CharField(source_field="user_type", max_length=2, null=True, default="00",
                                describe="用户类型（00系统用户）")
    email = fields.CharField(max_length=50, null=True, unique=True, default="", describe="用户邮箱")
    phonenumber = fields.CharField(max_length=11, null=True, unique=True, default="", describe="手机号码")
    sex = fields.CharField(max_length=1, null=True, default="0", describe="用户性别（0男 1女 2未知）")
    avatar = fields.CharField(max_length=100, null=True, default="", describe="头像地址")
    password = fields.CharField(max_length=100, null=True, default="", describe="密码")
    status = fields.CharField(max_length=1, null=True, default="0", describe="帐号状态（0正常 1停用）")
    delFlag = fields.CharField(source_field="del_flag", max_length=1, null=True, default="0",
                               describe="删除标志（0代表存在 2代表删除）")
    loginIp = fields.CharField(source_field="login_ip", max_length=128, null=True, default="", describe="最后登录IP")
    loginDate = fields.DatetimeField(source_field="login_date", null=True, describe="最后登录时间")
    remark = fields.CharField(max_length=500, null=True, describe="备注")

    class Meta:
        table = "sys_user"
        table_description = "用户信息表"


class SysUserRoleDb(Model):
    userId = fields.BigIntField(source_field="user_id", describe="用户ID")
    roleId = fields.BigIntField(source_field="role_id", describe="角色ID")

    class Meta:
        table = 'sys_user_role'
        table_description = "用户和角色关联表"


class SysUserPostDb(Model):
    userId = fields.BigIntField(source_field="user_id", describe="用户ID")
    postId = fields.BigIntField(source_field="post_id", describe="岗位ID")

    class Meta:
        table = 'sys_user_post'
        table_description = "用户与岗位关联表"
