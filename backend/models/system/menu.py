#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 15:28
# file: menu.py
# author: lotus163


from tortoise import fields

from backend.models.base import BaseTimeModel


class SysMenuDb(BaseTimeModel):
    menuId = fields.BigIntField(source_field="menu_id", pk=True, describe="菜单ID")
    menuName = fields.CharField(source_field="menu_name", max_length=50, describe="菜单名称")
    parentId = fields.BigIntField(source_field="parent_id", null=True, default=0, describe="父菜单ID")
    orderNum = fields.IntField(source_field="order_num", null=True, default=0, describe="显示顺序")
    path = fields.CharField(max_length=200, null=True, default="", describe="路由地址")
    component = fields.CharField(max_length=255, null=True, describe="组件路径")
    query = fields.CharField(max_length=255, null=True, describe="路由参数")
    isFrame = fields.IntField(source_field="is_frame", null=True, default=1, describe="是否为外链（0是 1否）")
    isCache = fields.IntField(source_field="is_cache", null=True, default=0, describe="是否缓存（0缓存 1不缓存）")
    menuType = fields.CharField(source_field="menu_type", max_length=1, null=True, default="",
                                describe="菜单类型（M目录 C菜单 F按钮）")
    visible = fields.CharField(max_length=1, null=True, default="0", describe="菜单状态（0显示 1隐藏）")
    status = fields.CharField(max_length=1, null=True, default="0", describe="菜单状态（0正常 1停用）")
    perms = fields.CharField(max_length=100, null=True, describe="权限标识")
    icon = fields.CharField(max_length=100, null=True, default="#", describe="菜单图标")
    remark = fields.CharField(max_length=500, null=True, describe="备注")

    class Meta:
        table = "sys_menu"
        table_description = "菜单权限表"
