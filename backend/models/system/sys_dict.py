#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 16:08
# file: sys_dict.py
# author: lotus163

from tortoise import fields

from backend.models.base import BaseTimeModel


class SysDictTypeDb(BaseTimeModel):
    dictId = fields.IntField(source_field="dict_id", pk=True, describe="字典主键")
    dictName = fields.CharField(source_field="dict_name", max_length=100, null=True, default="",
                                describe="字典名称")
    dictType = fields.CharField(source_field="dict_type", max_length=100, null=True, default="",
                                describe="字典类型")
    status = fields.CharField(max_length=1, null=True, default="0", describe="状态（0正常 1停用）")
    remark = fields.CharField(max_length=500, null=True, describe="备注")

    class Meta:
        table = "sys_dict_type"
        table_description = "字典类型表"


class SysDictDataDb(BaseTimeModel):
    dictCode = fields.IntField(source_field="dict_code", pk=True, describe="字典编码")
    dictSort = fields.IntField(source_field="dict_sort", null=True, default=0, describe="字典排序")
    dictLabel = fields.CharField(source_field="dict_label", max_length=100, null=True, default="",
                                 describe="字典标签")
    dictValue = fields.CharField(source_field="dict_value", max_length=100, null=True, default="",
                                 describe="字典键值")
    dictType = fields.CharField(source_field="dict_type", max_length=100, null=True, default="",
                                describe="字典类型")

    cssClass = fields.CharField(source_field="css_class", max_length=100, null=True, describe="样式属性（其他样式扩展）")
    listClass = fields.CharField(source_field="list_class", max_length=100, null=True, describe="表格回显样式")
    isDefault = fields.CharField(source_field="is_default", max_length=1, null=True, default="N",
                                 describe="是否默认（Y是 N否）")
    status = fields.CharField(max_length=1, null=True, default="0", describe="状态（0正常 1停用）")
    remark = fields.CharField(max_length=500, null=True, describe="备注")

    class Meta:
        table = "sys_dict_data"
        table_description = "字典数据表"
