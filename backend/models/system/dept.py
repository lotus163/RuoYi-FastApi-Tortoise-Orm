#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/1 15:28
# file: dept.py
# author: lotus163
from tortoise import fields

from backend.models.base import BaseTimeModel


class SysDeptDb(BaseTimeModel):
    deptId = fields.BigIntField(source_field="dept_id", pk=True, describe="部门id")
    parentId = fields.BigIntField(source_field="parent_id", null=True, default=0, describe="父部门id")
    ancestors = fields.CharField(max_length=50, null=True, default="", describe="祖级列表")
    deptName = fields.CharField(source_field="dept_name", max_length=30, null=True, default="", describe="部门名称")
    orderNum = fields.BigIntField(source_field="order_num", null=True, default=0, describe="显示顺序")
    leader = fields.CharField(max_length=20, null=True, describe="负责人")
    phone = fields.CharField(max_length=11, null=True, describe="联系电话")
    email = fields.CharField(max_length=50, null=True, describe="邮箱")
    status = fields.CharField(max_length=1, null=True, default="0", describe="部门状态（0正常 1停用）")
    delFlag = fields.CharField(source_field="del_flag", max_length=1, null=True, default="0",
                               describe="删除标志（0代表存在 2代表删除）")

    class Meta:
        table = "sys_dept"
        table_description = "部门表"
