#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File: datascope.py
# @Author: lotus163
# @Date: 2022/11/24
from typing import List

from backend.core.custom_exc import PermissionNotEnough


class DataScope:

    def __init__(self, scopes: List[int]):
        self.scopes = scopes

    def get_sopes(self, datas: List[int]) -> List[int]:
        return list(set(datas) & set(self.scopes))

    def has_sope(self, data: int):
        if data not in self.scopes:
            raise PermissionNotEnough()
