#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/28 14:18
# file: casbin.py
# author: lotus163
from typing import List

from casbin import Enforcer


class CasbinService:
    def __init__(self, e: Enforcer):
        self.e : Enforcer= e

    def has_permission(self, userName: str, obj: str, act: str) -> bool:
        return self.e.enforce(userName, obj, act)

    async def add_role_for_user(self,userName:str,roleName:str):
        await self.e.add_role_for_user(userName,roleName)

    async def add_roles_for_user(self,userName:str,roleNames:List[str]):
        for role_name in roleNames:
            await self.add_role_for_user(userName,role_name)

    async def delete_role_for_user(self,userName:str,roleName:str):
        await self.e.delete_role_for_user(userName,roleName)

    async def delete_roles_for_user(self,userName:str):
        await self.e.delete_roles_for_user(userName)

    async def update_roles_for_user(self,userName:str,roleNames:List[str]):
        await self.delete_roles_for_user(userName)
        await self.add_roles_for_user(userName,roleNames)

    async def add_policy_for_role(self,roleName:str,obj:str,act:str):
        await self.e.add_policy(roleName,obj,act)

    async def delete_policy_for_role(self,roleName:str):
        await self.e.delete_permissions_for_user(roleName)

    async def delete_user(self,userName):
        await self.e.delete_user(userName)

    async def delete_role(self,roleName):
        await self.e.delete_role(roleName)


