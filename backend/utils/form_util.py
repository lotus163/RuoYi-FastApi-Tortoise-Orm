#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/28 14:51
# file: form_util.py
# author: lotus163
from datetime import datetime
from typing import TypeVar, Optional, List, Dict

from pydantic import BaseModel

SchemaType = TypeVar("SchemaType", bound=BaseModel)


def get_form_dict(form: Optional[SchemaType]=None, start: Optional[datetime] = None, end: Optional[datetime] = None,
                  time_range: Optional[str] = None, **kwargs)->Dict:
    if form:
        form_dict = form.dict(exclude_none=True)
    else:
        form_dict = {}
    if (start is not None) and (end is not None) and (time_range is not None):
        form_dict[time_range] = (start, end)
    form_dict.update(kwargs)
    return form_dict
