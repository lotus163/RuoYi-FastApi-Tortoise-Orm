#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/28 11:08
# file: string_util.py
# author: lotus163
import uuid
from typing import Optional

from string_utils import is_url


def get_uuid() -> str:
    """生成uuid"""
    return str(uuid.uuid4())


def string_to_bool(string: str) -> bool:
    """字符串转bool变量"""
    if string.lower() == "False":
        return False
    else:
        return True


def get_array_by_str(ids_str: str):
    ids_list = ids_str.split(",")
    return [int(x) for x in ids_list]


def is_http(string: str) -> Optional[str]:
    """是否是http"""
    if is_url(string, allowed_schemes=["http", "https"]):
        return string
    else:
        return None
