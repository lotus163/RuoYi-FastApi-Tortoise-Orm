#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/17 16:30
# file: login_infor.py
# author: lotus163
from backend.models import LoginInforDb
from backend.schemas import LoginInforSch


class LoginInforService:

    async def add_login_infor(self, form: LoginInforSch):
        form_dict = form.dict(exclude_none=True)
        await LoginInforDb.create(**form_dict)
