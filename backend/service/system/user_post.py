#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/17 14:51
# file: user_post.py
# author: lotus163
from typing import List, Optional

from ...models import SysUserPostDb


class UserPostService:

    async def get_post_ids_by_user_id(self, userId: int) -> Optional[List[int]]:
        # 获取userId的角色列表
        user_posts_db = await SysUserPostDb.filter(userId=userId).all()
        post_ids = [user_post.postId for user_post in user_posts_db]
        return list(set(post_ids))

    async def add_posts_by_user_id(self, userId: int, postIds: List[int]):
        user_post_dbs = []
        for post_id in postIds:
            user_post_dbs.append(SysUserPostDb(userId=userId, postId=post_id))
        await SysUserPostDb.bulk_create(user_post_dbs)

    async def delete_posts_by_user_id(self, userId: int):
        await SysUserPostDb.filter(userId=userId).delete()

    async def update_posts_by_user_id(self, userId: int, postIds: List[int]):
        await self.delete_posts_by_user_id(userId)
        await self.add_posts_by_user_id(userId, postIds)
