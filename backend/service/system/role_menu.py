#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/17 11:31
# file: role_menu.py
# author: lotus163
from typing import List

from backend.models import SysRoleMenuDb


class RoleMenuService:

    async def get_menu_ids_by_role_ids(self, roleIds: List[int]) -> List[int]:
        role_menu_dbs = await SysRoleMenuDb.filter(roleId__in=roleIds).all()
        menu_ids = [role_menu_db.menuId for role_menu_db in role_menu_dbs]
        return list(set(menu_ids))

    async def get_menu_ids_by_role_id(self, roleId: int) -> List[int]:
        role_menu_dbs = await SysRoleMenuDb.filter(roleId=roleId).all()
        menu_ids = [role_menu_db.menuId for role_menu_db in role_menu_dbs]
        return list(set(menu_ids))

    async def add_menus_by_role_id(self, roleId: int, menuIds: List[int]):
        role_menu_dbs = []
        for menu_id in menuIds:
            role_menu_dbs.append(SysRoleMenuDb(roleId=roleId, menuId=menu_id))
        await SysRoleMenuDb.bulk_create(role_menu_dbs)

    async def delete_menus_by_role_id(self, roleId: int):
        await SysRoleMenuDb.filter(roleId=roleId).delete()

    async def update_menus_by_role_id(self, roleId: int, menuIds: List[int]):
        await self.delete_menus_by_role_id(roleId)
        await self.add_menus_by_role_id(roleId, menuIds)
