#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File: sys_config.py
# @Author: lotus163
# @Date: 2022/10/30

from loguru import logger

from backend.core.redis import MyRedis, get_config_key
from backend.crud import SysConfigDao


class SysConfigService:

    def __init__(self, redis: MyRedis):
        self.redis = redis

    async def loading_config_cache(self):
        """载入config到redis"""
        # 从数据库读取sys_config
        configs = await SysConfigDao.get_list_db()
        # 循环写入redis
        for config in configs:
            await self.redis.set(get_config_key(config.configKey), config.configValue)
        logger.info("redis存储sys_config成功。")
