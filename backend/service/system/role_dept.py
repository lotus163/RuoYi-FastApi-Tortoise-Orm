#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File: role_dept.py
# @Author: lotus163
# @Date: 2022/11/17
from typing import List

from ...models import SysRoleDeptDb


class RoleDeptService:

    async def get_dept_ids_by_role_id(self, roleId: int) -> List[int]:
        role_depts_db = await SysRoleDeptDb.filter(roleId=roleId).all()
        dept_ids = [role_dept.deptId for role_dept in role_depts_db]
        return list(set(dept_ids))

    async def add_depts_by_role_id(self, roleId: int, deptIds: List[int]):
        role_dept_dbs = []
        for dept_id in deptIds:
            role_dept_dbs.append(SysRoleDeptDb(roleId=roleId, deptId=dept_id))
        await SysRoleDeptDb.bulk_create(role_dept_dbs)

    async def delete_depts_by_role_id(self, roleId: int):
        await SysRoleDeptDb.filter(roleId=roleId).delete()

    async def update_depts_by_role_id(self, roleId: int, deptIds: List[int]):
        await self.delete_depts_by_role_id(roleId)
        await self.add_depts_by_role_id(roleId, deptIds)

    async def get_dept_ids_by_role_ids(self, roleIds: List[int]) -> List[int]:
        role_depts_db = await SysRoleDeptDb.filter(roleId__in=roleIds).all()
        dept_ids_temp = []
        for role_dept_db in role_depts_db:
            dept_ids_temp.append(role_dept_db.deptId)
        return list(set(dept_ids_temp))
