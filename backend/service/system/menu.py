#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/27 19:28
# file: menu.py
# author: lotus163
from typing import List

from backend.crud import MenuDao
from backend.models import SysMenuDb
from backend.schemas import SysMenuSch, SysMenuTreeSch


class MenuService:

    async def get_permissions_list_by_menu_ids(self,menuIds:List[int])->List[str]:
        menu_dbs = await MenuDao.get_list_db(menuId__in = menuIds)
        permissions_list = []
        for menu_db in menu_dbs:
            permissions_list.append(menu_db.perms)
        return permissions_list

    async def get_menus_tree(self, menus_sch: List[SysMenuSch])-> List[SysMenuTreeSch]:
        return self.list_to_tree(menus_sch, parent_id=0)

    def list_to_tree(self, menu_schs: List[SysMenuSch], parent_id=0) -> List[SysMenuTreeSch]:
        """
        递归获取树形结构数据

        :param data_list: 数据列表
        :param parent_id: 父级id
        :return:
        """
        tree = []
        for menu_sch in menu_schs:
            if menu_sch.parentId == parent_id:
                dept_tree = SysMenuTreeSch(id=menu_sch.menuId, label=menu_sch.menuName)
                tree.append(dept_tree)
                dept_tree.children = self.list_to_tree(menu_schs, dept_tree.id)
        return tree
