#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/11/28 15:53
# file: role.py
# author: lotus163
from typing import Optional, List

from backend.crud import RoleDao
from backend.schemas import SysRoleSch
from backend.service.system.dept import DeptService
from backend.service.system.menu import MenuService
from backend.service.system.role_dept import RoleDeptService
from backend.service.system.role_menu import RoleMenuService


class RoleService:

    async def get_role_all_sch_by_id(self,roleId:int,user_dept_id:int)->Optional[SysRoleSch]:
        """获取角色sch的全部内容"""
        role_sch = await RoleDao.get_sch_or_none(roleId=roleId)
        if role_sch:
            role_sch.menuIds = await RoleMenuService().get_menu_ids_by_role_id(role_sch.roleId)
            role_sch.permissions =await MenuService().get_permissions_list_by_menu_ids(role_sch.menuIds)
            if role_sch.dataScope == "1":
                role_sch.deptIds = await DeptService().get_all_dept_ids()
            elif role_sch.dataScope == "2":
                role_sch.deptIds = await RoleDeptService().get_dept_ids_by_role_id(role_sch.roleId)
            elif role_sch.dataScope == "3":
                role_sch.deptIds = [user_dept_id]
            elif role_sch.dataScope == "4":
                role_sch.deptIds = await DeptService().get_dept_ids_and_children(user_dept_id)
            else:
                role_sch.deptIds = []
            return role_sch
        else:
            return None

    async def get_role_all_sch_by_ids(self,roleIds:List[int],user_dept_id:int)->Optional[List[SysRoleSch]]:
        roles_list = []
        for role_id in roleIds:
            roles_list.append(await self.get_role_all_sch_by_id(role_id,user_dept_id))
        return roles_list


